import { Article } from "../models/article";
import trainImage from "../assets/images/train.png";
import peopleImage from "../assets/images/people.png";

const articles: Article[] = [
  {
    title: "CO2 emissions: Train versus plane",
    description:
      "When it comes to carbon dioxide (CO2) emissions, many people wonder if taking a train or a plane is better for the environment. The answer to this...",
    image: trainImage,
  },
  {
    title: "The Benefits of Composting and How to Get Started",
    description:
      "Composting is the process of decomposing organic materials into a nutrient-rich soil-like substance that can be used as a fertilizer for...",
    image: peopleImage,
  },
  {
    title: "CO2 emissions: Train versus plane",
    description:
      "When it comes to carbon dioxide (CO2) emissions, many people wonder if taking a train or a plane is better for the environment. The answer to this...",
    image: trainImage,
  },
  {
    title: "The Benefits of Composting and How to Get Started",
    description:
      "Composting is the process of decomposing organic materials into a nutrient-rich soil-like substance that can be used as a fertilizer for...",
    image: peopleImage,
  },
  {
    title: "CO2 emissions: Train versus plane",
    description:
      "When it comes to carbon dioxide (CO2) emissions, many people wonder if taking a train or a plane is better for the environment. The answer to this...",
    image: trainImage,
  },
  {
    title: "The Benefits of Composting and How to Get Started",
    description:
      "Composting is the process of decomposing organic materials into a nutrient-rich soil-like substance that can be used as a fertilizer for...",
    image: peopleImage,
  },
];

const useArticles = (): { articles: Article[] } => ({
  articles,
});

export default useArticles;
