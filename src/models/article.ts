export type Article = {
  title: string;
  description: string;
  image: string;
};
