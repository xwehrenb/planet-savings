# Planet savings

## Getting Started

### Install Dependencies

Before running the application, you need to install the necessary dependencies.
Open your terminal, navigate to them project directory, and run the following
command:

```bash
npm install
```

This will install all the dependencies defined in `package.json`.

### Running the Application in Development Mode

To start the application in development mode, run the following command in your
terminal:

```bash
npm run dev
```

## Working with Icons

Icons are included in the `src/assets/icons` directory and can be easily
imported and used in your components thanks to SVGR. Here's how you can load
and use an SVG icon in your React component:

```jsx
import GlobeIcon from "./assets/icons/globe.svg?react";

function YourComponent() {
  return (
    <div>
      <GlobeIcon />
    </div>
  );
}
```

## Working with Images

Images are stored in the `src/assets/images` directory. You can import and
use them in your components like this:

```jsx
import trainImage from "./assets/images/trainImage.png";

function YourComponent() {
  return <img src={yourImageName} alt="Description" />;
}
```

## Mocking Articles with `useArticles` Hook

To simulate fetching a list of articles, you can use the `useArticles`
hook. This hook provides a simple way to access a mock list of articles
in your components. Here's an example of how to use it:

```jsx
import { useArticles } from "./hooks/useArticles";

function ArticlesComponent() {
  const { articles } = useArticles();

  return (
    <div>
      {articles.map((article) => ( /* render card */ ))}
    </div>
  );
}
```

This will render a list of articles, each with a title and content.

## Lib clsx

This project is configured to use `clsx`, a tiny utility for constructing
`className` strings conditionally. It's especially useful when you need
to apply multiple classes to a component based on certain conditions.
Here's a quick example:

```jsx
import clsx from "clsx";

function Button({ disabled, children }) {
  return (
    <button className={clsx("btn", { "btn--disabled": disabled })}>
      {children}
    </button>
  );
}
```

---

We hope this guide helps you get started with your new project smoothly.
If you have any questions or encounter any issues, feel free to ask for
help.

Happy coding!
